var renderer = new PIXI.autoDetectRenderer();
var app = new Application({
	width: 800, 
	height: 600,
	antialias: false
});
document.body.appendChild(app.view);

loader
	.add([
		"/assets/sampleSpritesheet.json"
	])
	.on("progress", loadProgressHandler)
	.load(setup);

function loadProgressHandler(loader, resource){
	console.log("loading " + resource.url + ".............." + loader.progress + "%");
}

//Managers
var scManager, 
	eManager, 
	pManager, 
	bManager, 
	scenes, 
	eSpawner, 
	aObserver,
	scoreTracker;

//Control keys
const KEY_ASCII = [69, // SHOOT
				  65, // RIGHT
				  87, // UP
				  68, // LEFT
				  83  // DOWN
				];

//Scene Keys
const GAME_SCENE = 0,
	  GAMEOVER_SCENE = 1,
	  CHAR_SCENE = 2;
//Textures
const SPRITE_SHEET = "/assets/sampleSpritesheet.json",
	  BACKGROUND_TEXTURE = "desert-bg.png",
	  BULLET_TEXTURE = "bulletB01.png";


var areaBox = {x: 0, y: 105, width: 979, height: 582};
var playerArea = {x: 0, y: 105, width: 979, height: 582};

//Characters list
//Ria - vanilla
//Von - sniper, slow and frail
//Den - shotgun, probably OP
const CHARACTERS = [{
		name: "Ria",
		hp:100,
		bulletSpeed:8,
		bulletDamage:5,
		speed:5,
		texture:"ria-walking01.png"
	},
	{
		name:"Von",
		hp:30,
		bulletSpeed:10,
		bulletDamage:8,
		speed:2,
		texture:"von-walking01.png"
	},
	{
		name:"Den",
		hp:50,
		bulletSpeed:5,
		bulletDamage:10,
		speed:4,
		texture: "den-walking01.png"
	}
];

const ENEMIES = [{
		name:"CowboySlime",
		hp:50,
		bulletSpeed:5,
		bulletDamage:5,
		maxTimer: 100,
		speed:4,
		points:10,
		texture: "cowboyslime.png"
	},
	{
		name:"Golem",
		hp:50,
		bulletSpeed:0,
		bulletDamage:29,
		maxTimer: 0,
		speed:1,
		points:10,
		texture: "golem-walking01.png"
	},
	{
		name:"RedSlime",
		hp:50,
		bulletSpeed:8,
		bulletDamage:2,
		maxTimer: 200,
		speed:0,
		points:10,
		texture: "cowboyslimered.png"
	}
];

//Variables for Game
var player,
	bullets,
	enemies, 
	state,
	sheetID;

//Variables for char selection
var selectedChar, selectedBox;

var achBox;

function setup(){
	//Set spritesheet texture	
	sheetID = loader.resources[SPRITE_SHEET].textures;

	//Set background
	var tilingSprite = new TilingSprite(sheetID[BACKGROUND_TEXTURE], renderer.width, renderer.height);
	app.stage.addChild(tilingSprite);

	//Initialize managers, observers
	aObserver = new AchievementObserver();
	eManager = new EnemyManager();
	pManager = new PlayerManager();
	bManager = new BulletManager();
	scoreTracker = new ScoreTracker();

	//Initialize variables
	selectedChar = 0;	
	enemies = [];
	bullets = [];
	state = play;

	//Create scenes
	scenes = [];

	var scene = new GameScene();
	app.stage.addChild(scene);
	scene.visible = false;
	scenes.push(scene);

	scene = new GameOverScene();
	app.stage.addChild(scene);	
	scene.visible = false;
	scenes.push(scene);

	scene = new CharSelectScene();
	app.stage.addChild(scene);	
	scene.visible = true;
	scenes.push(scene);

	//Initialize Scene Manager
	scManager = new SceneManager(scenes[CHAR_SCENE]);

	//Initialize Enemy Spawner
	eSpawner = new EnemySpawner();
	
	//Set variables
	player = new Player();

	//instantiate scenes
	scenes[GAME_SCENE].instantiate();
	scenes[GAMEOVER_SCENE].instantiate();
	scenes[CHAR_SCENE].instantiate();

	//Achievement
	achBox = new AchievementBox();
	achBox.instantiate();

	var keys = [new keyboard(KEY_ASCII[0]), //Shoot
				new keyboard(KEY_ASCII[1]), //Up
				new keyboard(KEY_ASCII[2]), //D
				new keyboard(KEY_ASCII[3]), //L
				new keyboard(KEY_ASCII[4])
				];

	assignKeyActions(keys);

	app.ticker.add(delta => gameLoop(delta));
}

function gameLoop(delta){
	state(delta);
}

function play(delta){
	if(scManager.currScene == scenes[GAME_SCENE]){
		//update Entities
		pManager.updatePlayer(player);
		bManager.updateBullets(bullets);
		eManager.updateEnemies(enemies);

		//update Achivement box
		achBox.update();

		//update Display HP
		scenes[GAME_SCENE].update();

		//Observer for Achievements
		aObserver.observe(achBox);

		//start random enemy spawn
		eSpawner.randomSpawn();
		
		//check collision with stage
		eManager.checkCollisionWithWall(enemies);
		pManager.checkCollisionWithWall(player);
		bManager.checkCollisionWithWall(bullets);

		//check collision of bullets with player
		bManager.checkCollisionWithPlayer(bullets,player);

		//check collision of enemies to player
		eManager.checkCollisionWithPlayer(enemies,player);

		//check collision of bullets with enemies
		bManager.checkCollisionWithEnemies(bullets,enemies);

		if(player.stats.currHP<=0){
			scenes[GAMEOVER_SCENE].setScore(scoreTracker.score);
			scManager.goToScene(scenes[GAMEOVER_SCENE]);
		}
	}
	
}

function initPlayer(sChar){
	var p = new Player(sChar.name,
						sheetID[sChar.texture],
						sChar.hp,
						sChar.speed,
						sChar.bulletSpeed,
						sChar.bulletDamage
						);
	p.x=130;
	p.y=130;
	p.vx=0;
	p.vy=0;
	return p;
}

function assignKeyActions(keys){


	keys[0].press = () => {
		
		if(scManager.currScene == scenes[GAME_SCENE]){
			spawnBullet(1,player,1);			
			aObserver.detectedShoot();
			scenes[GAME_SCENE].addChild(achBox);
		}
		if(scManager.currScene == scenes[CHAR_SCENE]){
			//get selected character and go to game
			player = initPlayer(CHARACTERS[selectedChar]);			
			scenes[GAME_SCENE].instantiate();	
			scManager.goToScene(scenes[GAME_SCENE]);
		}
	};
	keys[0].release = () => {
		
	};
	keys[1].press = () => {
		if(scManager.currScene == scenes[GAME_SCENE]){
			player.vx = -player.speed;
			player.vy = 0;
		}
		if(scManager.currScene == scenes[CHAR_SCENE]){
			if(selectedBox.x>0){
				selectedBox.x-=200;
				selectedChar -= 1;
			}
		}
	};
	keys[1].release = () => {
		if(scManager.currScene == scenes[GAME_SCENE]){
			if (!keys[3].isDown && player.vy === 0) {
				player.vx = 0;
			}
		}
	};
	keys[2].press = () => {
		if(scManager.currScene == scenes[GAME_SCENE]){
			player.vy = -player.speed;
			player.vx = 0;
		}
	};
	keys[2].release = () => {
		if(scManager.currScene == scenes[GAME_SCENE]){
			if (!keys[4].isDown && player.vx === 0) {
				player.vy = 0;
			}
		}
		
	};
	keys[3].press = () => {
		if(scManager.currScene == scenes[GAME_SCENE]){
		player.vx = player.speed;
		player.vy = 0;
		}
		if(scManager.currScene == scenes[CHAR_SCENE]){
			if(selectedBox.x<400){
				selectedBox.x+=200;
				selectedChar += 1;
			}
		}
	};
	keys[3].release = () => {
		if(scManager.currScene == scenes[GAME_SCENE]){
			if (!keys[1].isDown && player.vy === 0) {
				player.vx = 0;
			}
		}
	};

	//keyS
	keys[4].press = () => {
		if(scManager.currScene == scenes[GAME_SCENE]){
			player.vy = player.speed;
			player.vx = 0;
		}
	};
	keys[4].release = () => {
		if(scManager.currScene == scenes[GAME_SCENE]){
			if (!keys[2].isDown && player.vx === 0) {
				player.vy = 0;
			}
		}
	};

}