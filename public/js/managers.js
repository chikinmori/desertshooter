//Managers check collision with walls, updates (x,y) position, enemy action
function EnemyManager(){
	this.checkCollisionWithWall = function(enemies){
		enemies.forEach(function(enemy){
			let hitsWall = contain(enemy, areaBox);
			if (hitsWall === "top" || hitsWall === "bottom") {
			    enemy.vy *= -1;
			 }
		});
	};
	this.updateEnemies = function(enemies){
		enemies.forEach(function(enemy){
			enemy.update();
			enemy.move();
		});
	};
	this.checkCollisionWithPlayer = function(enemies,player){
		for(var i=0; i<enemies.length; i++){
			if(hitTestRectangle(enemies[i],player)){
					enemies[i].hit(player);
					scenes[GAME_SCENE].removeChild(enemies[i]);
					enemies.splice(i,1);
				}
		}
	};

}

function BulletManager(){
	this.checkCollisionWithWall = function(bullets){
		for(var i=0; i<bullets.length; i++){
			let hitsWall = contain(bullets[i], areaBox);
			if (hitsWall === "right" || hitsWall === "left" || hitsWall === "top" || hitsWall === "bottom") {
			    bullets[i].vx=0;
			    scenes[GAME_SCENE].removeChild(bullets[i]);
			    bullets.splice(i,1);
			 }
		}
	};
	this.updateBullets = function(bullets){
		bullets.forEach(function(bullet){
			bullet.update();
		});
	};
	this.checkCollisionWithPlayer = function(bullets,player){
		for(var i=0; i<bullets.length; i++){
			if(hitTestRectangle(bullets[i],player) && bullets[i].team==0){
					player.stats.currHP-=bullets[i].damage;
					scenes[GAME_SCENE].removeChild(bullets[i]);
					bullets.splice(i,1);
			}
		}
	};
	this.checkCollisionWithEnemies = function(bullets,enemies){
		for(var i=0; i<bullets.length; i++){			
			for(var j=0; j<enemies.length; j++){
				var hit = hitTestRectangle(bullets[i],enemies[j]);
				if(hit && bullets[i].team==1){
					enemies[j].hit(bullets[i]);
					scenes[GAME_SCENE].removeChild(bullets[i]);
					bullets[i].x=areaBox.x+1;
					bullets[i].y=areaBox.width;
					if(enemies[j].stats.currHP<=0){
						scenes[GAME_SCENE].removeChild(enemies[j]);
						scoreTracker.addScore(enemies[j].points);
						enemies.splice(j,1);
						aObserver.addKills();
					}

				}
			}
		}
	};
}

function PlayerManager(){
	this.checkCollisionWithWall = function(player){
		let hitsWall = contain(player, playerArea);
		if (hitsWall === "top" || hitsWall === "bottom" || hitsWall === "right" || hitsWall === "left" ) {
			player.vy = 0;
		}
	};
	this.updatePlayer = function(player){
		player.update();
	};
}

//Tracks current scene
function SceneManager(initScene){
	this.currScene = initScene;

	//Traverse to next scene
	this.goToScene = function(nextScene){
		this.currScene.visible = false;
		nextScene.visible = true;
		this.currScene = nextScene;
	};
}

//Randomly picks an enemy type
//Spawns at random x,y
function EnemySpawner(){
	this.randomSpawn = function(){
		var ranSelect = Math.floor((Math.random() * 3) + 1);
		var randX = Math.floor((Math.random() * 2) + 1);
		var randY = Math.floor((Math.random() * 4) + 1);
		if(enemies.length<=3){
			if(ranSelect==1)
				spawnEnemy(600 + (70*randX), 150 + (50*randY), new BasicEnemy());
			else if(ranSelect==2)
				spawnEnemy(550 + (70*randX), 150 + (50*randY), new ShadyEnemy());
			else
				spawnEnemy(680, 150 + (50*randY), new RabidEnemy());
		}
	};
	
}

//Tracks score
function ScoreTracker(){
	this.score = 0;
	this.addScore = function(score){
		this.score += score;
	};
}