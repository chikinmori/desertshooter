//List of achievements

var achievementsList = [
		{name: "Learned how to shoot", completed: false},
		//Press E to shoot
		{name: "The Exterminator", completed: false},
		//Kill 10 Enemies
		{name: "I'm a Survivor", completed: false}
		//Leave character to one 1 HP
		//Easiest way to do this is choose Von and get hit by the Golem
	];

//stores variables needed for all achievements
//sees if the conditions are met
function AchievementObserver(){
	this.shot = false;
	this.kills = 0;

	this.detectedShoot = function(){
		this.shot = true;
	};

	this.addKills = function(){
		this.kills++;
	};

	this.oneHP = function(p){
		if(p.stats.currHP == 1)
			return true;
		else
			return false;
	};

	//params - AchievementBox Object
	//add conditions for each achievement
	this.observe = function(aBox){
		if(this.kills>=10)
			aBox.triggerAchievement(achievementsList[1]);
		if(this.shot)
			aBox.triggerAchievement(achievementsList[0]);
		if(this.oneHP(player))
			aBox.triggerAchievement(achievementsList[2]);
	};
}

//Achievement Box
//run triggerAchievement( achievement ) to trigger animation
function AchievementBox(){
	Container.call(this);
	this.achievement;
	this.currTimer=0;
	this.maxTimer=60;
	this.triggerAnimation = false;
	this.triggerDisplay = false;
	this.instantiate = function(){
		var rect = new Graphics();
		rect.beginFill(0xaaf354);
		rect.drawRect(0,0,200,60);
		this.addChild(rect);

		var text = new Text("Achievement Get!", styleA);
		text.x = 10;
		text.y = 10;
		this.addChild(text);
		this.achievement = new Text("", styleC);
		this.achievement.x = 10;
		this.achievement.y = 30;
		this.addChild(this.achievement);
		this.x=580;
		this.y=-60;
		this.vx=0;
		this.vy=0;
	};
	this.triggerAchievement = function(ach){
		if(!ach.completed){
			ach.completed=true;
			this.achievement.text=ach.name;
			this.triggerAnimation = true;
		}		
	};
	this.update = function(){
		this.x+=this.vx;
		this.y+=this.vy;
		if(this.triggerAnimation){
			if(this.currTimer<this.maxTimer){
				this.vy=1;
				this.currTimer++;
			}
			else if(this.currTimer>=this.maxTimer){
				this.vy=0;
				this.currTimer=0;
				this.triggerAnimation = false;
				this.triggerDisplay = true;
				this.maxTimer=100;
			}
		}
		if(this.triggerDisplay){
			if(this.currTimer<this.maxTimer){
				this.currTimer++;
			}
			else if(this.currTimer>=this.maxTimer){
				this.y=-60;
				this.triggerDisplay = false;
				this.maxTimer=60;
				this.currTimer=0;
			}
		}


	};
}

AchievementBox.prototype = Object.create(Container.prototype);

