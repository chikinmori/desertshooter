var styleA = new TextStyle({
		fontFamily: "Arial",
		fontSize: 18,
		fill: 0x000000
	}
);
var styleC = new TextStyle({
		fontFamily: "Arial",
		fontSize: 15,
		fill: 0x000000
	}
);
var styleD = new TextStyle({
		fontFamily: "Arial",
		fontSize: 15,
		fill: 0xffffff
	}
);
var styleB = new TextStyle({
		fontFamily: "Arial",
		fontSize: 25,
		fill: 0x000000
	}
);

function GameScene(){
	Container.call(this);
	this.hpText;
	this.score;
	this.instantiate = function(){
		this.addChild(player);
		this.hpText = new Text("",styleC);
		this.hpText.x=0;
		this.hpText.y=0;
		this.addChild(this.hpText);
	};
	this.update = function(){
		this.hpText.text = player.stats.currHP + "/" + player.stats.maxHP;
	};
}
GameScene.prototype = Object.create(Container.prototype);

function GameOverScene(){
	Container.call(this);
	this.scoreText;
	this.instantiate = function(){
		var rect = new Graphics();
		rect.beginFill(0x000000);
		rect.drawRect(0,0,app.width,app.height);
		this.addChild(rect);

		var style = new TextStyle({
				fontFamily: "Arial",
				fontSize: 25,
				fill: 0xffffff
			}
		);
		this.scoreText = new Text("", styleB);
		this.addChild(this.scoreText);
	};
	this.setScore = function(score){
		this.scoreText.text = "Final Score: " + score ;
	};
}
GameOverScene.prototype = Object.create(Container.prototype);


function CharSelectScene(){
	Container.call(this);
	this.instantiate = function(){

		selectedBox = new Graphics();
		selectedBox.beginFill(0xe5926c);
		selectedBox.drawRect(70,180,200,350);
		this.addChild(selectedBox);

		var playerSprite = new Sprite(sheetID[CHARACTERS[0].texture]);
		playerSprite.x = 150;
		playerSprite.y =300;
		this.addChild(playerSprite);

		playerSprite = new Sprite(sheetID[CHARACTERS[1].texture]);
		playerSprite.x = 350;
		playerSprite.y =300;
		this.addChild(playerSprite);

		playerSprite = new Sprite(sheetID[CHARACTERS[2].texture]);
		playerSprite.x = 550;
		playerSprite.y =300;
		this.addChild(playerSprite);

		var text = new Text("Select Character", styleB);
		text.x = 300;
		text.y = 140;
		this.addChild(text);
		text = new Text("Ria", styleB);
		text.x = 150;
		text.y = 200;
		this.addChild(text);
		text = new Text("Von", styleB);
		text.x = 350;
		text.y = 200;
		this.addChild(text);
		text = new Text("Den", styleB);
		text.x = 550;
		text.y = 200;
		this.addChild(text);
	};
}
CharSelectScene.prototype = Object.create(Container.prototype);

