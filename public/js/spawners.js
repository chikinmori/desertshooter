

function spawnBullet(direction,source,team){
	var n = 1;
	var offset = 0 ;
	if(source.name== CHARACTERS[2].name || source.name == ENEMIES[2].name){
		n = 3;
		offset = -30;
	}
	for(var i=0; i<n; i++){
		var bullet = new Bullet("bullet",5);
		bullet.team = team || 0;
		bullet.speed = source.bulletSpeed;
		bullet.damage = source.bulletDamage;
		bullet.x=source.x + (50*direction);
		bullet.y=source.y + source.height/2 + (30*i) + offset;
		bullet.vx=bullet.speed * direction;
		bullet.vy=0;
		bullets.push(bullet);
		scenes[GAME_SCENE].addChild(bullet);
	}
	
}

function spawnEnemy(x,y,enemy){
	enemy.x=x;
	enemy.y=y;
	enemy.vx=0;
	enemy.vy=enemy.speed;
	enemies.push(enemy);
	scenes[GAME_SCENE].addChild(enemy);
}