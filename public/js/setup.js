var type = "WebGL";
if(!PIXI.utils.isWebGLSupported()){
	type = "canvas";
}
PIXI.utils.sayHello(type);

PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;

//aliases
var Application = PIXI.Application,
	loader = PIXI.loader,
	resources = PIXI.loader.resources,
	Sprite = PIXI.Sprite,
	TextureCache = PIXI.utils.TextureCache,
	Rectangle = PIXI.Rectangle,
	Graphics = PIXI.Graphics,
	Text = PIXI.Text,
	TextStyle = PIXI.TextStyle,
	AnimatedSprite = PIXI.extras.AnimatedSprite,
	TilingSprite = PIXI.extras.TilingSprite,
	Container = PIXI.Container;