function Entity(name, texture){
	Sprite.call(this, texture);
	this.name = name;
	this.speed = 0;
	this.team = 0;
	this.update= function(){
		this.x += this.vx;
		this.y += this.vy;
	};
}

Entity.prototype = Object.create(Sprite.prototype);
function Player(name, texture, hp, speed, bulletSpeed, bulletDamage){
	Entity.call(this, name, texture);
	this.stats = {
		currHP: hp,
		maxHP: hp
	};
	this.speed = speed;
	this.bulletSpeed = bulletSpeed;
	this.bulletDamage = bulletDamage;
	this.team=1;
}

Player.prototype = Object.create(Entity.prototype);

function Bullet(name, damage){
	Entity.call(this, name, sheetID[BULLET_TEXTURE]);
	this.damage = damage;
}

Bullet.prototype = Object.create(Entity.prototype);

function Enemy(name, texture, hp, bd, bs, s, p){
	Entity.call(this, name, texture);
	this.stats = {
		currHP: hp,
		maxHP: hp,
	};
	this.bulletDamage = bd;
	this.bulletSpeed = bs;
	this.speed = s;
	this.points = p;
	this.move = function(){};
}

Enemy.prototype = Object.create(Entity.prototype);

function BasicEnemy(){
	Enemy.call(this,ENEMIES[0].name,sheetID[ENEMIES[0].texture], 
				ENEMIES[0].hp,
				ENEMIES[0].bulletDamage,
				ENEMIES[0].bulletSpeed,
				ENEMIES[0].speed,
				ENEMIES[0].points);
	this.currTimer = 0;
	this.maxTimer = ENEMIES[0].maxTimer;
	this.move = function(){
		if(this.currTimer >= this.maxTimer){
			spawnBullet(-1,this);
			this.currTimer = 0;
		}
		else{
			this.currTimer++;
		}
		
	};
	this.hit = function(target){
		if(target.name == "bullet")
			this.stats.currHP -= target.damage;
	};
}
BasicEnemy.prototype = Object.create(Enemy.prototype);

function RabidEnemy(){
	Enemy.call(this,ENEMIES[1].name,sheetID[ENEMIES[1].texture], 
				ENEMIES[1].hp,
				ENEMIES[1].bulletDamage,
				ENEMIES[1].bulletSpeed,
				ENEMIES[1].speed,
				ENEMIES[1].points);
	this.move = function(){
		var tx = player.x - this.x,
		    ty = player.y - this.y,
		    dist = Math.sqrt(tx*tx+ty*ty);
		  
		this.vx = (tx/dist)*this.speed;
		this.vy = (ty/dist)*this.speed;
	};
	this.hit = function(target){
		if(target.name=="bullet"){
			this.stats.currHP-=target.damage;
		}
		else if(target.name == CHARACTERS[0].name || target.name == CHARACTERS[1].name || target.name == CHARACTERS[2].name){
			target.stats.currHP-=this.bulletDamage;
		}
	};
}
RabidEnemy.prototype = Object.create(Enemy.prototype);

function ShadyEnemy(){
	Enemy.call(this,ENEMIES[2].name,sheetID[ENEMIES[2].texture], 
				ENEMIES[2].hp,
				ENEMIES[2].bulletDamage,
				ENEMIES[2].bulletSpeed,
				ENEMIES[2].speed,
				ENEMIES[2].points);
	this.currTimer=0;
	this.maxTimer=ENEMIES[2].maxTimer;
	this.move = function(){
		if(this.currTimer>=this.maxTimer){
			spawnBullet(-1,this);
			this.currTimer=0;
		}
		else{
			this.currTimer++;
		}
		
	};
	this.hit = function(target){
		if(target.name=="bullet")
			this.stats.currHP-=target.damage;
	};
}
ShadyEnemy.prototype = Object.create(Enemy.prototype);